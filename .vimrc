let g:pathogen_disabled = []
if !has('gui_running')
	call add(g:pathogen_disabled, 'YouCompleteMe')
endif

call pathogen#infect()
call pathogen#helptags()

set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*/node_modules/*

set shiftwidth=4
set tabstop=4
set expandtab
autocmd FileType go setlocal shiftwidth=2 tabstop=2 expandtab!
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2

set smartindent
set hlsearch
set incsearch
set noswapfile

filetype plugin indent on
syntax on

set guifont=Menlo:h10

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

colorscheme koehler

set splitbelow
set splitright

" CtrlP
let g:ctrlp_working_path_mode = ''

" vim-go
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_operators = 1

" YCM
if has("gui_running")
	let g:ycm_autoclose_preview_window_after_insertion = 1
endif

" vim-jsbeautify
autocmd FileType javascript nnoremap <buffer> <f2> :call JsBeautify()<cr>
autocmd FileType html nnoremap <buffer> <f2> :call HtmlBeautify()<cr>
autocmd FileType css nnoremap <buffer> <f2> :call CSSBeautify()<cr>
